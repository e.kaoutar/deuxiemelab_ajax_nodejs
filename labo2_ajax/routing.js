const express = require('express')
const app = express()
const PORT = 5000
const bodyParser = require('body-parser')
const mysql = require('mysql')
const multer = require('multer')
const path = require('path')


//---------------------Multer issues-------------------------

//set storage engine
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function(request, file, cb){
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))}
})

//initialize our upload variable
const upload = multer({
    storage: storage
    //limits: {fileSize: 10}, //facultatif : limiting the size of files uploaded (by bytes)
    //fileFilter: (request, file, cb) => {
      //  checkFileType(file, cb)
    //}
}).single('myImage')

// checkFileType : function definition
function checkFileType(file, cb){
    //ext restrictions
    const filetypes = /jpeg|jpg|png|gif/
    //check the extention
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
    //check mime
    const mimetype = filetypes.test(file.mimetype)

    if(mimetype && extname)
    {
        return cb(null, true)
    }
    else{
        cb('Error : Images Only!')
    }
}

app.post('/upload', (request, response) => {
    //we have to call upload method
    upload(request, response, (err) =>{
        if(err){
            response.render('uploading', {msg : err})
        }
        else{
            
            if(request.file == undefined){
                response.render('uploading', {msg : 'Error : No picture selected!'})
            }
            else{
                response.render('uploading', {msg : 'File uploaded!', file: `assets/uploads/${request.file.filename}` })
            }
            console.log(request.file)
        }
    })
})
//-------------------------------------------------------

//----------------------Conexion init-------------------------
let connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'nodemysql'
  })
  
connection.connect()
//-------------------------------------------------------------

//using ejs
app.set('view engine', 'ejs')

//------------------Mes middlewares ----------------------------------

//distribution of the static folder that contains all style files
app.use('/assets', express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
//------------------------------------------------------------


//tableau statique a afficher
let tablangues = [{id_lng:'', name_lng:'', racine:'', item2_menu:'', item3_menu:'', item4_menu:''}]
let laLangue=''

//-----------------Mes routes get-----------------------------------

app.get('/about', (request, response) => {

    let SQL = `SELECT * FROM langages WHERE langue='${laLangue}'`
        let query = connection.query(SQL, (err, results) =>{
            if(err) throw err
            response.render('pages/aboutHome', {langues : results})
        })
})

//redirection vers la page services
app.get('/services', (request, response)=>{

    let SQL = `SELECT * FROM langages WHERE langue='${laLangue}'`
    let query = connection.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('pages/servicesHome', {langues : results})
    })

})

//redirection vers la page contact
app.get('/contact', (request, response)=>{

    let SQL = `SELECT * FROM langages WHERE langue='${laLangue}'`
    let query = connection.query(SQL, (err, results) =>{
        if(err) throw err
        response.render('pages/contactHome', {langues : results})
    })
})

app.get('/success', (request, response)=>{
    response.render('pages/success')
})

app.get('/home', (request, response) => {
    let SQL = `SELECT * FROM langages WHERE langue='${laLangue}'`
    let query = connection.query(SQL, (err, rows) =>{
        if(err) throw err
        response.render('pages/home', {langues : rows})
        })
})

app.get('/', (request, response) => {
    response.render('pages/admin')
})

app.get('/upload', (request, response) => {
    response.render('uploading')
})
//--------------------------------------------------------------


//--------------------------MES POSTS---------------------------

app.post('/', (request, response) => {
    
    if(request.body.user == 'kaoutar' && request.body.pass == 'kaoutar'){
        //response.render('pages/success')
            //un select des langages a mettre dans le select de la vue
        let SQL = "SELECT * FROM langages"
        let query = connection.query(SQL, (err, results) =>{
            if(err) throw err
            response.render('pages/index', {langues : results})
        })
    }
    else
    {
        response.render('pages/failed')
    }
    

})

app.post('/access', (request, response, next)=>{//sans ajax
    
    let SQL = `SELECT * FROM langages WHERE langue='${request.body.pickLng}'`
    laLangue = request.body.pickLng
    let query = connection.query(SQL, (err, rows) =>{
        if(err) throw err
        response.render('pages/home', {langues : rows})
        })
})

app.post('/succeed', (request, response) => {//avec ajax
    let sql = 'INSERT INTO langages SET id_lng=?, langue=?, racine=?, item2_menu=?, item3_menu=?, item4_menu=?'
    connection.query(sql, [request.body.id_lng, request.body.name_lng, request.body.home_lng, request.body.about_lng, request.body.service_lng, request.body.contact_lng], 
    (err, result) => {
        //reponse envoye cote client
        response.send("yes")
    })
    
})

//--------------------------------------------------------------

//connection test done successfully
app.listen(PORT, () => {
    console.log(`Server started at port: ${PORT}`)
})